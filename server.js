//Inicializamos el framework en esta variables constante
const express = require('express');
const app = express();
const port = process.env.PORT || 3000

const bodyParser = require('body-parser');
app.use(bodyParser.json());
app.listen(port);
console.log("API escuchando en el puerto " + port);

const userController = require('./controllers/userController');
const authController = require('./controllers/authController');

app.get('/apitechu/v1/hello',
// Funcion manejadora con request y response
  function(req, res){
    console.log("GET /apitechu/v1/hello");

    res.send({"msg" : "Hola desde API TechU con el cambio de node-modules"});
  }
);

app.post("/apitechu/v1/monstruo/:p1/:p2",
  function (req, res){
    console.log("POST /apitechu/v1/monstruo/:p1/:p2");

    console.log("Parametros");
    console.log(req.params);

    console.log("Query Strings");
    console.log(req.query);

    console.log("Headers");
    console.log(req.headers);

    console.log("Body");
    console.log(req.body);

  }
);

app.get('/apitechu/v1/users', userController.getUsersV1);
app.post('/apitechu/v1/users', userController.createUserV1);
app.delete('/apitechu/v1/users/:id', userController.deleteUserV1);
app.get('/apitechu/v2/users', userController.getUsersV2);
app.get('/apitechu/v2/users/:id', userController.getUserByIdV2);
app.post('/apitechu/v2/users', userController.createUserV2);

app.post('/apitechu/v1/login', authController.loginV1);
app.post('/apitechu/v1/logout', authController.logoutV1);

app.post('/apitechu/v2/login', authController.loginV2);
app.post('/apitechu/v2/logout/:id', authController.logoutV2);
