const io = require('../io');
const crypt = require('../crypt');
const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechujjc6ed/collections/";
const mLabAPIKey = "apiKey=tiUsmY7NXdPoFlSJDcFp0Hn1VZ6A_mdb";


function getUsersV1(req, res){
  console.log("GET /apitechu/v1/users");
  //Para recuperar los queryStrings
  console.log(req.query);

  //Devuelve el fichero json de usuarios
  //res.sendFile('usuarios.json',{root: __dirname});

  //Devuelve el fichero en una variable
  var users = require('../usuarios.json');
  var result = {};

  if (req.query.$count == "true") {
    console.log("Count needed");
    result.count = users.length;
  }

  result.users = req.query.$top ?
     users.slice(0, req.query.$top) : users;

  res.send(result);
}

function createUserV1(req, res){
  console.log("POST /apitechu/v1/users");
  //Las cabeceras que esta llegando desde la peticion cliente
  console.log("first_name es " + req.body.first_name);
  console.log("last_name es " + req.body.last_name);
  console.log("email es " + req.body.email);

  var newUser = {
    "first_name" : req.body.first_name,
    "last_name" : req.body.last_name,
    "email" : req.body.email
  };

  var users = require('../usuarios.json');
  users.push(newUser);
  //Se ha factorizado la escritura en fichero en una funcion
  io.writeUserDataToFile(users);

  console.log("Usuario añadido con éxito");
  res.send({"msg" : "Usuario añadido con éxito"});
}

function deleteUserV1(req, res) {
  console.log("DELETE /apitechu/v1/users/:id");
  console.log("id es " + req.params.id);

  var users = require('../usuarios.json');
  //Los arrays empiezan en cero
  //users.splice(req.params.id - 1, 1); //lo quita de la lista

  /*Mi solucion*/
  var valorId = req.params.id;
  for (user of users) {
      console.log(user.id);
      if (user != null && user.id == valorId) {
          //Para borrar con un contador y haces el splice de la posicion
          //users.splice(i, 1);
          delete users[user.id - 1];
          console.log("Usuario con id igual a " + valorId + " en la posicion " + user.id + " borrado");
          break;
      }
  }

  /*Solucion 1 del profe*/
  /*for (user of users){
    console.log("Length of array is " + users.length);
    if (user != null && user.id == req.params.id){
      console.log("La id coincide");
      delete users[user.id - 1];
      break;
    }
  }*/

  /*Solucion 2 profe*/
  /*for (arrayId in users) {
   console.log("posición del array es " + arrayId);
   if (users[arrayId].id == req.params.id) {
     console.log("La id coincide");
     users.splice(arrayId, 1);
     break;
   }
 }*/

  /*Solucion 3 profe, el forEach no tiene break*/
  /*users.forEach(function (user, index) {
  if (user.id == req.params.id) {
    console.log("La id coincide");
    users.splice(index, 1);
  }
  });*/

  //Hay que persistir en el fichero el cambio
  io.writeUserDataToFile(users);
  console.log("Usuario Borrado");
  res.send({"msg" : "Usuario borrado con éxito"});
}

/////VERSION2/////

function getUsersV2(req, res){
  console.log("GET /apitechu/v2/users");

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client Create");
  httpClient.get("user?" + mLabAPIKey,
    function (err, resMLab, body){
      var response = !err ? body : {
        "msg" : "Error obteniendo usuarios."
      }
      res.send(response);
    }
  );
}

function getUserByIdV2(req, res){
  console.log("GET /apitechu/v2/users/:id");

  var id = req.params.id;
  //'' comillas simples es un String literal
  var query  = 'q={"id":' + id + '}';

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client Create");
  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function (err, resMLab, body){
      if (err) {
        response = {
          "msg" : "Error obteniendo usuario"
        }
        res.status(500);
      } else {
        if (body.length > 0) {
          var response = body[0];
          //Por defecto el status es 200
        } else {
          var response = {
            "msg" : "Usuario no encontrado"
          }
          res.status(404);
        }
      }
      /*var response = !err ? body : {
        "msg" : "Error obteniendo usuario."
      }*/
      res.send(response);
    }
  );
}

function createUserV2(req, res) {
  console.log("POST /apitechu/v2/users");

  console.log("first_name es " + req.body.first_name);
  console.log("last_name es " + req.body.last_name);
  console.log("email es " + req.body.email);
  //console.log("password es " + req.body.password);

  var newUser = {
    "first_name" : req.body.first_name,
    "last_name" : req.body.last_name,
    "email" : req.body.email,
    "password" : crypt.hash(req.body.password)
  };

  //Queremos insertar un documento (INSERT DOCUMENT) ver la documentacion del API de MLAB
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client Create");
  httpClient.post("user?" + mLabAPIKey, newUser,
    function (err, resMLab, body){
      console.log("Usuario creado con exito");
      res.send({"msg" : "Usuario creado con exito"})
    }
  )
}


module.exports.getUsersV1 = getUsersV1;
module.exports.createUserV1 = createUserV1;
module.exports.deleteUserV1 = deleteUserV1;
module.exports.getUsersV2 = getUsersV2;
module.exports.getUserByIdV2 = getUserByIdV2;
module.exports.createUserV2 = createUserV2;
